import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import UserList from "./components/UserList/UserList";
import FormInput from "./components/FormInput/FormInput";
import FormSelect from "./components/FormInput/FormSelect";
import MButton from "./components/MButton/MButton";
import UserJobs from "./components/Jobs/Jobs";
import JobType from "./components/JobType/JobType";

const App = () => {
  // ! Userlarni saqlash uchun
  const [users, setUsers] = useState([]);
  // ! Userlarni api-dan olish uchun
  const getUsers = () => {
    axios
      .get("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user")
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // ! Component ishga tushganda userlarni olish
  useEffect(() => {
    getUsers();
  }, []);
  // ! Userni delete qilish uchun
  const deleteUser = (id) => {
    axios
      .delete(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user/${id}`) // ! Hardoyim ham id bolib kelmaydi
      .then((res) => {
        getUsers();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // ! Formni dastlabki value-lari
  const intialValues = {
    user_info: {
      firstName: '',
      lastName: '',
      email: '',
      phone_number: '',
      dob: ''
    },
    work_area: {
      company_name: '',
      job_type: '',
      experience: ''
    }
  }
  const [values, setValues] = useState(intialValues);

  // ! Formni submit qilish
  const onSubmit = (e) => {
    e.preventDefault();
    const formData = {
      user_info: {
        firstName: values.firstName,
        lastName: values.lastName,
        email: values.email,
        phone_number: values.phone_number,
        dob: values.dob
      },
      work_area: {
        company_name: values.company_name,
        job_type: values.job_type,
        experience: values.experience
      }
    }
    // ! axios.post() yangi user qo'shish uchun
    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user", formData)
      .catch((err) => console.log("err", err))
      .finally(() => {
        getUsers();
        setValues(intialValues);
        window.location = window.location.href
      });
  };
  const OnClear = (e) => {
    e.preventDefault();
    window.location = window.location.href
  }
  
  const [jobs, setJobs] = useState([]);
  const getJobs = () => {
    axios
      .get("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types")
      .then((res) => {
        setJobs(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getJobs();
  }, []);

  const deleteJob = (id) => {
    axios
      .delete(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types/${id}`) // ! Hardoyim ham id bolib kelmaydi
      .then((res) => {
        getUsers();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  

  const onSubmit12 = (e) => {
    e.preventDefault();

    const dev = { value: values.developer, label: values.developer }

    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types", dev)
      .catch((err) => console.log("err", err))
      .finally(() => {
        getJobs();
        window.location = window.location.href
      });


  }
  // ! HINT FOR FUTURE:
  const forexperience = [
    { value: "Select", label: 'Select...' },
    { value: "1", label: '1' },
    { value: "2", label: '2' },
    { value: "3", label: '3' },
    { value: "4", label: '4' },
    { value: "5", label: '5' }
  ];
  let chiziq = <hr style={{ borderTop: '1px ', width: '99%', marginBottom: '40px' }} />
  return (
    <>
      <div className="main">
        <div className="main-two">
          <div className="formWrapper sty">
            <h1 className="left">User's Info</h1>{chiziq}

            <form className="form" id="Ice" onSubmit={onSubmit} >
              <FormInput
                required // ! required berilmasa validation bu polya uchun ishlamaydi
                pattern="^[a-zA-Z]{3,20}$" // ! Validation uchun regex
                label="First name" // ! Label inputni tepasidagi text uchun
                placeholder="Will"
                name="firstName"
                value={values.firstName} // ! Value berilmasa handleChange ishlamaydi
                handleChange={
                  (e) => setValues({ ...values, firstName: e.target.value }) // ! Input value-sini o'zgartirish uchun
                }
                errorMessage="First name is required"
              />
              <FormInput
                required // ! required berilmasa validation bu polya uchun ishlamaydi
                pattern="^[a-zA-Z]{3,20}$" // ! Validation uchun regex
                label="Last name" // ! Label inputni tepasidagi text uchun
                placeholder="Smith"
                name="lastName"
                value={values.lastName} // ! Value berilmasa handleChange ishlamaydi
                handleChange={
                  (e) => setValues({ ...values, lastName: e.target.value }) // ! Input value-sini o'zgartirish uchun
                }
                errorMessage="Last name is required"
              />
              <FormInput
                type="email"
                pattern={
                  "^[a-zA-Z0-9.!#$%&'*+/=? ^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$"
                }
                errorMessage="Email is required"
                required
                label="Email"
                placeholder="exemple@gmail.com"
                name="email"
                value={values.email}
                handleChange={(e) =>
                  setValues({ ...values, email: e.target.value })
                }
              />
              <FormInput
                type="phone_number"
                label="Phone number"
                placeholder="+998998007070"
                name="phone_number"
                required
                errorMessage="Phone number is required"
                pattern="^[0-9+]{9,13}$"
                value={values.phone_number}
                handleChange={(e) =>
                  setValues({ ...values, phone_number: e.target.value })
                }
              />
              <FormInput
                required
                type="date"
                label="Date of birth"
                name="dob"
                errorMessage="Date is required"
                value={values.dob}
                handleChange={(e) =>
                  setValues({ ...values, dob: e.target.value })
                }
              />
            </form>
          </div>
          <div className="main-right">
            <div className="sty job">
              <h1 className="left">Work Details</h1>{chiziq}
              <form className="form" onSubmit={onSubmit}>
                <FormInput
                  required // ! required berilmasa validation bu polya uchun ishlamaydi
                  pattern="^[a-zA-Z]{3,20}$" // ! Validation uchun regex
                  placeholder="Udevs"
                  label="Company Name" // ! Label inputni tepasidagi text uchun
                  name="company_name"
                  value={values.company_name} // ! Value berilmasa handleChange ishlamaydi
                  handleChange={
                    (e) => setValues({ ...values, company_name: e.target.value }) // ! Input value-sini o'zgartirish uchun
                  }
                  errorMessage="Company Name is required"
                />
                <div className="work">
                  <FormSelect
                    required
                    optionss={jobs}
                    label="Job Type"
                    name="job_type"
                    value={values.job_type} // ! Value berilmasa handleChange ishlamaydi
                    handleChange={
                      (e) => setValues({ ...values, job_type: e.target.value }) // ! Input value-sini o'zgartirish uchun
                    }
                    errorMessage="Job Type is required"
                  />
                  <FormSelect
                    required
                    label="Experience" // ! Label inputni tepasidagi text uchun
                    optionss={forexperience}
                    name="experience"
                    value={values.experience} // ! Value berilmasa handleChange ishlamaydi
                    handleChange={
                      (e) => setValues({ ...values, experience: e.target.value }) // ! Input value-sini o'zgartirish uchun
                    }
                    errorMessage="Experience is required"
                  />
                </div>
              </form>
            </div>
            <div className="sty job">
              <h1 className="left">Job Type</h1> {chiziq}
              <form className="form" onSubmit={onSubmit12}>
                <JobType
                  required // ! required berilmasa validation bu polya uchun ishlamaydi
                  pattern="^[a-zA-Z]{3,20}$" // ! Validation uchun regex
                  placeholder="Developer"
                  label="Label" // ! Label inputni tepasidagi text uchun
                  name="developer"
                  value={values.developer} // ! Value berilmasa handleChange ishlamaydi
                  handleChange={
                    (e) => setValues({ ...values, developer: e.target.value }) // ! Input value-sini o'zgartirish uchun
                  }
                  errorMessage="Developer is required"
                />
                <div className="button">
                  <MButton
                    BSize={"submit"}
                    type="clear"
                  >
                    Отменить
                  </MButton>
                  <MButton
                    BClass={"button-prm"}
                    BSize={"submit"}
                    type="submit"
                  >
                    Сохранить
                  </MButton>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="button">
          <MButton
            OnClick={OnClear}
          >
            Отменить
          </MButton>
          <MButton
            OnClick={onSubmit}
            BSize={"submit"}
            type="submit"
            BClass={"button-prm"}
          >
            Сохранить
          </MButton>
        </div>
      </div>
      {/* // ! Userlist */}
      <UserList users={users} deleteUser={deleteUser} />
      <UserJobs jobs={jobs} deleteJob={deleteJob} />
    </>
  );
};

export default App;
