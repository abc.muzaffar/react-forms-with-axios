import React from "react";
import { ReactComponent as ActionIcon } from "../../Assets/Images/action.svg";
import { ReactComponent as DeleteIcon } from "../../Assets/Images/delete.svg";
import MButton from "../MButton/MButton";
import "./userList.css";

const UserList = ({ users, deleteUser }) => {

  return (
  
    <div className="userList">
      <h1>Users</h1>
      <div className="static default">
        <div className="item">Full name</div>
        <div className="item">Date of birth</div>
        <div className="item">Phone</div>
        <div className="item">Email</div>
        <div className="item">Company Name</div>
        <div className="item">Job Type</div>
        <div className="item">Experience</div>
        <ActionIcon className="item action action__btn" />
      </div>
      {users &&
        users.map((user) => (
          <div className="default" key={user?.id}>
            <div className="item">{user.user_info?.firstName + " " + user.user_info?.lastName}</div>
            <div className="item">{user.user_info?.dob}</div>
            <div className="item">{user.user_info?.phone_number}</div>
            <div className="item">{user.user_info?.email}</div>
            <div className="item">{user.work_area?.company_name}</div>
            <div className="item">{user.work_area?.job_type}</div>
            <div className="item">{user.work_area?.experience}</div>
            <MButton
              onClick={() => {
                console.log('delete user', user?.id);
                deleteUser(user?.id)
              }}
              BClass="item action action__delete"
              BType={"outline-primary"}
              BIcon={<DeleteIcon />}
            />
          </div>
        ))}
    </div>
  );
};

export default UserList;
