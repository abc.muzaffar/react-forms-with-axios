import React from "react";
import "../../App.css";
import { ReactComponent as DeleteIcon } from "../../Assets/Images/delete.svg";
import MButton from "../MButton/MButton";


const UserJobs = ({ jobs, deleteJob }) => {
  return (
    <div className="sty resjob">
      <h1 className="left">Job Type</h1>
      <div className="result">
        {jobs &&
          jobs.map((job) => job.id && (
            <ul id={'ulid'+job.id}>
              <li className="min">{job?.id}</li>
              <li className="max">{job?.label}</li>
              <li className="min">
                <MButton
                  onClick={() => {
                    console.log('delete user', job?.id);
                    deleteJob(job?.id)
                    const id = document.getElementById('ulid'+job.id)
                    id.remove()
                  }}
                  BClass="item action action__delete btn-jobs"
                  BType={"outline-primary"}
                  BIcon={<DeleteIcon />}
                />
              </li>
            </ul>))}
      </div>
    </div>
  );
};

export default UserJobs;