import React, { useState } from "react";
import "./formInput.css";

const FormSelect = ({ name, label, optionss, required = false, errorMessage, onChange, handleChange, type, setSelectedValue, ...props }) => {
	const [touched, setTouched] = useState(false);
	return (
		<div className="selcl">
			{label && <label htmlFor={name}>{label}</label>}
			<select
				style={{ padding: "10px", minWidth: "250px", borderRadius: "6px", background: "white" }}
				onChange={handleChange}
				onFocus={() => name === 'confirmPassword' && setTouched(true)}
				touched={touched.toString()}
				required={required}
				name={name}
				{...props}
			>
				{optionss.map((option) => (
					<option key={option.value} value={option.value}>
						{option.label}
					</option>
				))}
			</select>
			{(required && errorMessage) && <div className="errorMessage">{errorMessage}</div>}
		</div>
	);
};

export default FormSelect;

